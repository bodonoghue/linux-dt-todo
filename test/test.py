#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-only
# Copyright 2022 Linaro Ltd

import os
import unittest

import dtbscheckparser

testdir = os.path.dirname(__file__)
datadir = os.path.join(testdir, 'data')

class TestLoadingTodoCompatibles(unittest.TestCase):
    def test_real_data(self):
        todos = dtbscheckparser.load_todos(os.path.join(testdir, '..', 'todo-compatibles.rst'))
        self.assertGreater(len(todos), 0)

    def test_empty(self):
        expected = {}
        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-compatibles-empty.rst'))
        self.assertEqual(todos, expected)

    def test_good(self):
        expected = {
            "['ad,ad7147_captouch']": 'ignore',
            "['adi,adau7002']": 'ignore',
            "['arm,kryo']": 'ignore',
            "['qcom,adc-tm7']": 'krzk, https://lore.kernel.org/all/b70df37b-e803-acca-e7e1-5b4526e76776@linaro.org/',
            "['qcom,apq8096-sndcard']": 'ignore',
        }
        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-compatibles-good.rst'))
        self.assertEqual(todos, expected)

    def test_missing_item(self):
        with self.assertRaises(ValueError) as context:
            todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-compatibles-missing-item.rst'))
        self.assertEqual(str(context.exception), "Line does not start with -:   ignore\n")

    def test_missing_status(self):
        with self.assertRaises(ValueError) as context:
            todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-compatibles-missing-status-1.rst'))
        self.assertEqual(str(context.exception),
                         "Line does not start with -:   krzk, https://lore.kernel.org/all/b70df37b-e803-acca-e7e1-5b4526e76776@linaro.org/\n")

        with self.assertRaises(ValueError) as context:
            todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-compatibles-missing-status-2.rst'))
        self.assertEqual(str(context.exception), "Missing second line for item: ['qcom,apq8096-sndcard']")

class TestLoadingTodoWarnings(unittest.TestCase):
    def test_real_data(self):
        todos = dtbscheckparser.load_todos(os.path.join(testdir, '..', 'todo-warnings.rst'))
        self.assertGreater(len(todos), 0)

    def test_empty(self):
        expected = {}
        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-warnings-empty.rst'))
        self.assertEqual(todos, expected)

    def test_good(self):
        expected = {
            "/: cpus:cpu@0: 'power-domain-names' is a required property": 'NEW',
            "/: cpus:cpu@0: 'power-domains' is a required property": 'NEW',
            "/: cpus:cpu@1: 'power-domain-names' is a required property": 'ignore',
            "/: cpus:cpu@1: 'power-domains' is a required property": 'ignore',
            "spmi@c440000: #address-cells:0:0: 2 was expected": 'krzk',
        }
        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-warnings-good.rst'))
        self.assertEqual(todos, expected)
    # Format and behavior is the same loading todo-compatibles, so everything is already tested in TestLoadingTodoCompatibles

class TestLoadingWarnings(unittest.TestCase):
    def test_empty(self):
        with self.assertRaises(ValueError) as context:
            dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-empty.log'),
                                                    'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(str(context.exception), "Could not find marker for DTBS warnings: arch/arm64/boot/dts/qcom:")

    def test_good(self):
        expected_compatibles = [
            "['ad,ad7147_captouch']",
            "['adi,adau7002']",
            "['arm,kryo']",
            "['awinic,aw8695']",
            "['dlg,da7280']",
            "['qcom,adc-tm7']",
            "['qcom,apq8096-sndcard']",
            "['qcom,apss-wdt-msm8994', 'qcom,kpss-wdt']",
        ]
        expected_warnings = [
            "spmi@c440000: #size-cells:0:0: 0 was expected",
            "spmi@c440000: #address-cells:0:0: 2 was expected",
            "pci@1c08000: Unevaluated properties are not allowed ('iommu-map', 'iommus' were unexpected)",
            "phy@1d87000: '#phy-cells' is a required property",
        ]
        (compatibles, warnings) = dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-good.log'),
                                                                          'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(compatibles, expected_compatibles)
        self.assertEqual(warnings, expected_warnings)

    def test_good_empty_warnings(self):
        expected_compatibles = [
            "['ad,ad7147_captouch']",
            "['adi,adau7002']",
            "['arm,kryo']",
            "['awinic,aw8695']",
            "['dlg,da7280']",
            "['qcom,adc-tm7']",
            "['qcom,apq8096-sndcard']",
            "['qcom,apss-wdt-msm8994', 'qcom,kpss-wdt']",
        ]
        expected_warnings = [
        ]
        (compatibles, warnings) = dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-good-empty-warnings.log'),
                                                                          'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(compatibles, expected_compatibles)
        self.assertEqual(warnings, expected_warnings)

    def test_good_empty_compatibles(self):
        expected_compatibles = [
        ]
        expected_warnings = [
            "spmi@c440000: #size-cells:0:0: 0 was expected",
            "spmi@c440000: #address-cells:0:0: 2 was expected",
            "pci@1c08000: Unevaluated properties are not allowed ('iommu-map', 'iommus' were unexpected)",
            "phy@1d87000: '#phy-cells' is a required property",
        ]
        (compatibles, warnings) = dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-good-empty-compatibles.log'),
                                                                          'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(compatibles, expected_compatibles)
        self.assertEqual(warnings, expected_warnings)

    def test_reversed(self):
        expected_warnings = [
            "['ad,ad7147_captouch']",
            "['adi,adau7002']",
            "['arm,kryo']",
            "['awinic,aw8695']",
            "['dlg,da7280']",
            "['qcom,adc-tm7']",
            "['qcom,apq8096-sndcard']",
            "['qcom,apss-wdt-msm8994', 'qcom,kpss-wdt']",
        ]
        expected_compatibles = [
            "spmi@c440000: #size-cells:0:0: 0 was expected",
            "spmi@c440000: #address-cells:0:0: 2 was expected",
            "pci@1c08000: Unevaluated properties are not allowed ('iommu-map', 'iommus' were unexpected)",
            "phy@1d87000: '#phy-cells' is a required property",
        ]
        with self.assertRaises(ValueError) as context:
            dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-reversed.log'),
                                                    'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(str(context.exception), "Could not match warning from line: ['ad,ad7147_captouch']")

    def test_no_qcom(self):
        with self.assertRaises(ValueError) as context:
            dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-good-no-qcom.log'),
                                                    'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(str(context.exception), "Could not find marker for DTBS warnings: arch/arm64/boot/dts/qcom:")

    def test_no_qcom_compatibles(self):
        with self.assertRaises(ValueError) as context:
            dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-good-no-qcom-compatibles.log'),
                                                    'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(str(context.exception), "Could not find marker for compatibles: arch/arm64/boot/dts/qcom:")

    def test_no_qcom_warnings(self):
        with self.assertRaises(ValueError) as context:
            dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-good-no-qcom-warnings.log'),
                                                    'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(str(context.exception), "Could not match warning from line: ['ad,ad7147_captouch']")

    def test_missing_number_before_dtbs(self):
        with self.assertRaises(ValueError) as context:
            dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-missing-number-before-dtbs-1.log'),
                                                    'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(str(context.exception), "Could not match warning from line: spmi@c440000: #address-cells:0:0: 2 was expected")

        with self.assertRaises(ValueError) as context:
            dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-missing-number-before-dtbs-2.log'),
                                                    'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(str(context.exception), "Could not match warning from line: spmi@c440000: #size-cells:0:0: 0 was expected")

    def test_good_next_20221010_arm(self):
        expected_compatibles = [
            "['8dev,jalapeno']",
            "['alfa-network,ap120c-ac']",
            "['brcm,bcm43430a0-bt']",
            "['brcm,bcm43430a1-bt']",
            "['coresight-etb10', 'arm,primecell']",
            "['linux,extcon-usb-gpio']",
            "['mikrotik,rb3011']",
            "['mx25l25635e']",
            "['panasonic,an30259a']",
            "['qcom,adc-tm7']",
            "['qcom,apq8064-ahci', 'generic-ahci']",
            "['qcom,apq8064-iommu']",
            "['qcom,apq8064-pinctrl']",
            "['qcom,apq8064-sata-phy']",
            "['qcom,apq8084-pinctrl']",
            "['qcom,arch-cache']",
            "['qcom,ci-hdrc']",
            "['qcom,crypto-v5.1']",
            "['qcom,idle-state-spc', 'arm,idle-state']",
            "['qcom,idle-state-spc']",
            "['qcom,ipq4019-pinctrl']",
            "['qcom,ipq4019-wifi']",
            "['qcom,ipq8064-pinctrl']",
            "['qcom,ipq806x-ahci', 'generic-ahci']",
            "['qcom,ipq806x-gmac', 'snps,dwmac']",
            "['qcom,ipq806x-sata-phy']",
            "['qcom,kpss-acc-v1']",
            "['qcom,kpss-acc-v2']",
            "['qcom,kpss-gcc', 'syscon']",
            "['qcom,kpss-timer', 'qcom,kpss-wdt-apq8064', 'qcom,msm-timer']",
            "['qcom,kpss-timer', 'qcom,kpss-wdt-ipq8064', 'qcom,msm-timer']",
            "['qcom,kpss-timer', 'qcom,kpss-wdt-msm8960', 'qcom,msm-timer']",
            "['qcom,kpss-timer', 'qcom,msm-timer']",
            "['qcom,lcc-apq8064']",
            "['qcom,lcc-ipq8064']",
            "['qcom,lcc-mdm9615']",
            "['qcom,lcc-msm8960']",
            "['qcom,lpass-cpu-apq8016']",
            "['qcom,mdm9615-pinctrl']",
            "['qcom,mdp5']",
            "['qcom,mdss']",
            "['qcom,msm-iommu-v1-ns']",
            "['qcom,msm-iommu-v1-sec']",
            "['qcom,msm8660-ebi2']",
            "['qcom,msm8660-pinctrl']",
            "['qcom,msm8916-acc']",
            "['qcom,msm8916-iommu', 'qcom,msm-iommu-v1']",
            "['qcom,msm8916-mss-pil', 'qcom,q6v5-pil']",
            "['qcom,msm8916-pinctrl']",
            "['qcom,msm8916-wcd-digital-codec']",
            "['qcom,msm8960-pinctrl']",
            "['qcom,msm8974-mss-pil']",
            "['qcom,msm8974-pinctrl']",
            "['qcom,nandcs']",
            "['qcom,pm8018-pwrkey', 'qcom,pm8921-pwrkey']",
            "['qcom,pm8058-keypad']",
            "['qcom,pm8058-keypad-led']",
            "['qcom,pm8058-led']",
            "['qcom,pm8058-pwrkey']",
            "['qcom,pm8916-wcd-analog-codec']",
            "['qcom,pm8921-keypad']",
            "['qcom,pm8921-pwrkey']",
            "['qcom,pm8941-iadc', 'qcom,spmi-iadc']",
            "['qcom,pronto-v2-pil', 'qcom,pronto']",
            "['qcom,riva-pil']",
            "['qcom,rpm-apq8064']",
            "['qcom,rpm-ipq8064']",
            "['qcom,rpm-mdm9615']",
            "['qcom,rpm-msm8660']",
            "['qcom,rpm-msm8960']",
            "['qcom,rpm-pm8018-regulators']",
            "['qcom,rpm-pm8058-regulators']",
            "['qcom,rpm-pm8901-regulators']",
            "['qcom,rpm-pm8921-regulators']",
            "['qcom,scss-timer', 'qcom,msm-timer']",
            "['qcom,sdx55-pdc', 'qcom,pdc']",
            "['qcom,sdx65-pdc', 'qcom,pdc']",
            "['qcom,ssbi']",
            "['qcom,wcn3620']",
            "['qcom,wcn3660']",
            "['qcom,wcn3660b']",
            "['qcom,wcn3680']",
            "['samsung,s6e3fa2']",
            "['semtech,sx1509q']",
            "['swir,mangoh-green-wp8548', 'swir,wp8548', 'qcom,mdm9615']",
            "['swir,mangoh-iotport-spi']",
            "['syna,rmi4-i2c']",
            "['ti,lp8556']",
        ]
        expected_warnings = [
            "/: memory: False schema does not allow {'device_type': ['memory'], 'reg': [[0, 0]]}",
            "idle-states: 'spc' does not match any of the regexes: '^(cpu|cluster)-', 'pinctrl-[0-9]+'",
            "syscon@f9011000: compatible: 'anyOf' conditional failed, one must be fixed:",
            "memory@fc428000: $nodename:0: 'memory@fc428000' does not match '^sram(@.*)?'",
            "/: cpus:cpu@1: 'power-domains' is a required property",
            "/: cpus:cpu@1: 'power-domain-names' is a required property",
            "/: cpus:cpu@0: 'power-domains' is a required property",
            "/: cpus:cpu@0: 'power-domain-names' is a required property",
            "pwrkey@800: Unevaluated properties are not allowed ('reg' was unexpected)",
            "mmc@12180000: Unevaluated properties are not allowed ('interrupt-names' was unexpected)",
            r"amba: $nodename:0: 'amba' does not match '^([a-z][a-z0-9\\-]+-bus|bus|localbus|soc|axi|ahb|apb)(@.+)?$'",
            "/: cpus:cpu@3: 'power-domains' is a required property",
            "/: cpus:cpu@3: 'power-domain-names' is a required property",
            "/: cpus:cpu@2: 'power-domains' is a required property",
            "/: cpus:cpu@2: 'power-domain-names' is a required property",
            "smd: rpm: Unevaluated properties are not allowed ('rpm-requests' was unexpected)",
            "power-controller@f90b9000: reg: [[4178284544, 4096], [4177563648, 4096]] is too long",
            "power-controller@f90b9000: '#power-domain-cells' is a required property",
            "power-controller@f90a9000: reg: [[4178219008, 4096], [4177563648, 4096]] is too long",
            "power-controller@f90a9000: '#power-domain-cells' is a required property",
            "power-controller@f9099000: reg: [[4178153472, 4096], [4177563648, 4096]] is too long",
            "power-controller@f9099000: '#power-domain-cells' is a required property",
            "power-controller@f9089000: reg: [[4178087936, 4096], [4177563648, 4096]] is too long",
            "power-controller@f9089000: '#power-domain-cells' is a required property",
            "power-controller@f9012000: compatible:0: 'qcom,saw2' is not one of ['qcom,sdm660-gold-saw2-v4.1-l2', 'qcom,sdm660-silver-saw2-v4.1-l2', 'qcom,msm8998-gold-saw2-v4.1-l2', 'qcom,msm8998-silver-saw2-v4.1-l2', 'qcom,msm8909-saw2-v3.0-cpu', 'qcom,msm8916-saw2-v3.0-cpu', 'qcom,msm8226-saw2-v2.1-cpu', 'qcom,msm8974-saw2-v2.1-cpu', 'qcom,apq8084-saw2-v2.1-cpu', 'qcom,apq8064-saw2-v1.1-cpu']",
            "power-controller@f9012000: compatible: ['qcom,saw2'] is too short",
            "power-controller@f9012000: 'regulator' does not match any of the regexes: 'pinctrl-[0-9]+'",
            "power-controller@f9012000: '#power-domain-cells' is a required property",
            "mmc@12400000: Unevaluated properties are not allowed ('interrupt-names' was unexpected)",
            "watchdog@b017000: compatible: 'oneOf' conditional failed, one must be fixed:",
        ]
        (compatibles, warnings) = dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-next-20221010-arm.log'),
                                                                          'arch/arm/boot/dts/', 'qcom')
        self.assertEqual(compatibles, expected_compatibles)
        self.assertEqual(warnings, expected_warnings)

    def test_good_next_20221010_arm64(self):
        expected_compatibles = [
            "['ad,ad7147_captouch']",
            "['adi,adau7002']",
            "['arm,kryo']",
            "['awinic,aw8695']",
            "['dlg,da7280']",
            "['fcs,fan53526']",
            "['focaltech,fts8719']",
            "['google,cheza', 'qcom,sdm845']",
            "['google,cheza-rev1', 'qcom,sdm845']",
            "['google,cheza-rev2', 'qcom,sdm845']",
            "['google,cr50']",
            "['jdi,fhd-nt35596s']",
            "['lattice,ice40-fpga-mgr']",
            "['linux,extcon-usb-gpio']",
            "['maxim,max98360a']",
            "['ovti,ov7251']",
            "['pwm-ir-tx']",
            "['qcom,adc-tm7']",
            "['qcom,apq8096-sndcard']",
            "['qcom,apss-wdt-msm8994', 'qcom,kpss-wdt']",
            "['qcom,ci-hdrc']",
            "['qcom,crypto-v5.1']",
            "['qcom,crypto-v5.4']",
            "['qcom,glink-rpm']",
            "['qcom,hfpll']",
            "['qcom,ipq6018-mdio', 'qcom,ipq4019-mdio']",
            "['qcom,ipq6018-wcss-pil']",
            "['qcom,ipq8074-pinctrl']",
            "['qcom,lpass-cpu-apq8016']",
            "['qcom,mdp5']",
            "['qcom,mdss']",
            "['qcom,msm-iommu-v1-ns']",
            "['qcom,msm-iommu-v1-sec']",
            "['qcom,msm8916-acc']",
            "['qcom,msm8916-iommu', 'qcom,msm-iommu-v1']",
            "['qcom,msm8916-mss-pil', 'qcom,q6v5-pil']",
            "['qcom,msm8916-pinctrl']",
            "['qcom,msm8916-wcd-digital-codec']",
            "['qcom,msm8992-pinctrl']",
            "['qcom,msm8994-pinctrl']",
            "['qcom,msm8996-mss-pil']",
            "['qcom,msm8996-pinctrl']",
            "['qcom,msm8998-mss-pil']",
            "['qcom,msm8998-pinctrl']",
            "['qcom,pm8916-wcd-analog-codec']",
            "['qcom,pronto-v2-pil', 'qcom,pronto']",
            "['qcom,qcs404-ethqos']",
            "['qcom,qcs404-pcie2-phy', 'qcom,pcie2-phy']",
            "['qcom,qcs404-pinctrl']",
            "['qcom,qcs404-turingcc']",
            "['qcom,rpm-msm8994']",
            "['qcom,sc7180-pinctrl']",
            "['qcom,sc7280-cpu-bwmon', 'qcom,msm8998-bwmon']",
            "['qcom,sc8280xp-aoss-qmp', 'qcom,aoss-qmp']",
            "['qcom,sc8280xp-cpufreq-epss', 'qcom,cpufreq-epss']",
            "['qcom,sc8280xp-ipcc', 'qcom,ipcc']",
            "['qcom,sc8280xp-pdc', 'qcom,pdc']",
            "['qcom,sdm630-pinctrl']",
            "['qcom,sdm630-smmu-v2', 'qcom,adreno-smmu', 'qcom,smmu-v2']",
            "['qcom,sdm630-smmu-v2', 'qcom,smmu-v2']",
            "['qcom,sdm660-pinctrl']",
            "['qcom,sdm845-lpasscc']",
            "['qcom,sdm845-mss-pil']",
            "['qcom,sdm845-pinctrl']",
            "['qcom,slim-ngd-v1.5.0']",
            "['qcom,slim-ngd-v2.1.0']",
            "['qcom,sm8150-camnoc-virt']",
            "['qcom,sm8150-ethqos']",
            "['qcom,sm8150-pinctrl']",
            "['qcom,sm8150-qmp-gen3x1-pcie-phy']",
            "['qcom,sm8150-qmp-gen3x2-pcie-phy']",
            "['qcom,sm8250-dpu']",
            "['qcom,sm8250-lpass-aoncc']",
            "['qcom,sm8250-mdss']",
            "['qcom,sm8350-cpufreq-epss', 'qcom,cpufreq-epss']",
            "['qcom,sm8450-cpufreq-epss', 'qcom,cpufreq-epss']",
            "['qcom,sm8450-pdc', 'qcom,pdc']",
            "['qcom,sm8450-tsens', 'qcom,tsens-v2']",
            "['qcom,soundwire-v1.3.0']",
            "['qcom,soundwire-v1.5.1']",
            "['qcom,soundwire-v1.6.0']",
            "['qcom,ufs_variant']",
            "['qcom,wcn3620']",
            "['qcom,wcn3660b']",
            "['qcom,wcn3990-wifi']",
            "['realtek,rt5682i']",
            "['removed-dma-pool', 'ramoops']",
            "['samsung,s6sy761']",
            "['silabs,si470x']",
            "['slim217,1a0']",
            "['syna,rmi4-i2c']",
            "['ti,drv2604']",
            "['ti,tas2552']",
            "['ti,tps65132']",
            "['truly,nt35597-2K-display']",
            "['visionox,rm69299-shift']",
        ]
        expected_warnings = [
            "spmi@c440000: #size-cells:0:0: 0 was expected",
            "spmi@c440000: #address-cells:0:0: 2 was expected",
            "power-controller@c300000: '#power-domain-cells' is a required property",
            "usb@a6f8800: 'dma-ranges' does not match any of the regexes: '^usb@[0-9a-f]+$', 'pinctrl-[0-9]+'",
            "watchdog@17c10000: Unevaluated properties are not allowed ('interrupts' was unexpected)",
            "video-codec@aa00000: 'operating-points-v2', 'opp-table' do not match any of the regexes: 'pinctrl-[0-9]+'",
            "spi@88dc000: Unevaluated properties are not allowed ('operating-points-v2', 'power-domains' were unexpected)",
            "leds@d800: interrupt-names: ['ovp'] is too short",
            "pmic@3: 'pwm' does not match any of the regexes: '(.*)?(wled|leds)@[0-9a-f]+$', '^adc-tm@[0-9a-f]+$', '^adc@[0-9a-f]+$', '^audio-codec@[0-9a-f]+$', '^mpps@[0-9a-f]+$', '^rtc@[0-9a-f]+$', '^temp-alarm@[0-9a-f]+$', '^vibrator@[0-9a-f]+$', 'extcon@[0-9a-f]+$', 'gpio(s)?@[0-9a-f]+$', 'pinctrl-[0-9]+', 'pon@[0-9a-f]+$', 'pwm@[0-9a-f]+$'",
            "pwmleds: 'keyboard-backlight' does not match any of the regexes: '^led(-[0-9a-f]+)?$', 'pinctrl-[0-9]+'",
            "proximity@28: 'label' does not match any of the regexes: 'pinctrl-[0-9]+'",
            "pmic@5: leds@d800:interrupts: [[5, 216, 1, 1]] is too short",
            "pmic@5: leds@d800:interrupt-names: ['ovp'] is too short",
            "leds@d800: interrupts: [[5, 216, 1, 1]] is too short",
            "spmi@c440000: Unevaluated properties are not allowed ('cell-index', 'pmic@0', 'pmic@1', 'pmic@4', 'pmic@5' were unexpected)",
            "pinctrl@3500000: qup-spi8-cs-gpio: {'pinmux': {'pins': ['gpio42', 'gpio43', 'gpio44'], 'function': ['qup12']}, 'pinmux-cs': {'pins': ['gpio45'], 'function': ['gpio']}} is not of type 'array'",
            "pinctrl@3500000: qup-spi5-cs-gpio: {'pinmux': {'pins': ['gpio25', 'gpio26', 'gpio27'], 'function': ['qup05']}, 'pinmux-cs': {'pins': ['gpio28'], 'function': ['gpio']}} is not of type 'array'",
            "pinctrl@3500000: qup-spi3-cs-gpio: {'pinmux': {'pins': ['gpio38', 'gpio39', 'gpio40'], 'function': ['qup03']}, 'pinmux-cs': {'pins': ['gpio41'], 'function': ['gpio']}} is not of type 'array'",
            "pinctrl@3500000: qup-spi11-cs-gpio: {'pinmux': {'pins': ['gpio53', 'gpio54', 'gpio55'], 'function': ['qup15']}, 'pinmux-cs': {'pins': ['gpio56'], 'function': ['gpio']}} is not of type 'array'",
            "pinctrl@3500000: qup-spi1-cs-gpio: {'pinmux': {'pins': ['gpio0', 'gpio1', 'gpio2'], 'function': ['qup01']}, 'pinmux-cs': {'pins': ['gpio3'], 'function': ['gpio']}} is not of type 'array'",
            "phy@88e3000: 'qcom,bias-ctrl-value', 'qcom,charge-ctrl-value', 'qcom,hsdisc-trim-value', 'qcom,imp-res-offset-value', 'qcom,preemphasis-level', 'qcom,preemphasis-width' do not match any of the regexes: 'pinctrl-[0-9]+'",
            "mdss@ae00000: 'displayport-controller@ae90000', 'dsi-phy@ae94400', 'dsi@ae94000' do not match any of the regexes: '^display-controller@[0-9a-f]+$', 'pinctrl-[0-9]+'",
            "soc@0: opp-table-qup: {'compatible': ['operating-points-v2'], 'phandle': [[53]], 'opp-75000000': {'opp-hz': [[0], [75000000]], 'required-opps': [[46]]}, 'opp-100000000': {'opp-hz': [[0], [100000000]], 'required-opps': [[48]]}, 'opp-128000000': {'opp-hz': [[0], [128000000]], 'required-opps': [[47]]}} should not be valid under {'type': 'object'}",
            "pinctrl@3500000: qup-spi0-cs-gpio: {'phandle': [[52]], 'pinmux': {'pins': ['gpio34', 'gpio35', 'gpio36'], 'function': ['qup00']}, 'pinmux-cs': {'pins': ['gpio37'], 'function': ['gpio']}, 'pinconf': {'pins': ['gpio34', 'gpio35', 'gpio36', 'gpio37'], 'drive-strength': [[2]], 'bias-disable': True}} is not of type 'array'",
            "lpass@62d87000: 'hdmi@5', 'mi2s@0', 'mi2s@1' do not match any of the regexes: '^dai-link@[0-9a-f]$', 'pinctrl-[0-9]+'",
            "dsi@ae94000: 'opp-table', 'vdda-supply' do not match any of the regexes: 'pinctrl-[0-9]+'",
            "pmic@1: 'pwm' does not match any of the regexes: '(.*)?(wled|leds)@[0-9a-f]+$', '^adc-tm@[0-9a-f]+$', '^adc@[0-9a-f]+$', '^audio-codec@[0-9a-f]+$', '^mpps@[0-9a-f]+$', '^rtc@[0-9a-f]+$', '^temp-alarm@[0-9a-f]+$', '^vibrator@[0-9a-f]+$', 'extcon@[0-9a-f]+$', 'gpio(s)?@[0-9a-f]+$', 'pinctrl-[0-9]+', 'pon@[0-9a-f]+$', 'pwm@[0-9a-f]+$'",
            "domain-idle-states: cluster-sleep-0: 'idle-state-name', 'local-timer-stop' do not match any of the regexes: 'pinctrl-[0-9]+'",
            "pci@1c08000: Unevaluated properties are not allowed ('iommu-map', 'iommus' were unexpected)",
            "phy@1d87000: '#phy-cells' is a required property",
        ]
        (compatibles, warnings) = dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-next-20221010-arm64.log'),
                                                                          'arch/arm64/boot/dts/', 'qcom')
        self.assertEqual(compatibles, expected_compatibles)
        self.assertEqual(warnings, expected_warnings)

class TestUpdateTodosCompatibles(unittest.TestCase):
    def test_empty_todos(self):
        expected = {
            "['ad,ad7147_captouch']": 'NEW',
            "['adi,adau7002']": 'NEW',
            "['arm,kryo']": 'NEW',
            "['awinic,aw8695']": 'NEW',
            "['dlg,da7280']": 'NEW',
            "['qcom,adc-tm7']": 'NEW',
            "['qcom,apq8096-sndcard']": 'NEW',
            "['qcom,apss-wdt-msm8994', 'qcom,kpss-wdt']": 'NEW',
        }

        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-compatibles-empty.rst'))
        (compatibles, warnings) = dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-good.log'),
                                                                          'arch/arm64/boot/dts/', 'qcom')
        new_todos = dtbscheckparser.update_todos(todos, compatibles)
        self.assertEqual(new_todos, expected)

    def test_empty_log(self):
        expected = {}

        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-compatibles-good.rst'))
        (compatibles, warnings) = ({}, {})
        new_todos = dtbscheckparser.update_todos(todos, compatibles)
        self.assertEqual(new_todos, expected)

    def test_empty_todos_and_log(self):
        expected = {}

        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-compatibles-empty.rst'))
        (compatibles, warnings) = ({}, {})
        new_todos = dtbscheckparser.update_todos(todos, compatibles)
        self.assertEqual(new_todos, expected)

    def test_good(self):
        expected = {
            "['ad,ad7147_captouch']": 'ignore',
            "['adi,adau7002']": 'ignore',
            "['arm,kryo']": 'ignore',
            "['awinic,aw8695']": 'NEW',
            "['dlg,da7280']": 'NEW',
            "['qcom,adc-tm7']": 'krzk, https://lore.kernel.org/all/b70df37b-e803-acca-e7e1-5b4526e76776@linaro.org/',
            "['qcom,apq8096-sndcard']": 'ignore',
            "['qcom,apss-wdt-msm8994', 'qcom,kpss-wdt']": 'NEW',
        }

        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-compatibles-good.rst'))
        (compatibles, warnings) = dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-good.log'),
                                                                          'arch/arm64/boot/dts/', 'qcom')
        new_todos = dtbscheckparser.update_todos(todos, compatibles)
        self.assertEqual(new_todos, expected)

class TestUpdateTodosWarnings(unittest.TestCase):
    def test_empty_todos(self):
        expected = {
            "pci@1c08000: Unevaluated properties are not allowed ('iommu-map', 'iommus' were unexpected)": 'NEW',
            "phy@1d87000: '#phy-cells' is a required property": 'NEW',
            "spmi@c440000: #size-cells:0:0: 0 was expected": 'NEW',
            "spmi@c440000: #address-cells:0:0: 2 was expected": 'NEW',
        }

        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-warnings-empty.rst'))
        (compatibles, warnings) = dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-good.log'),
                                                                          'arch/arm64/boot/dts/', 'qcom')
        new_todos = dtbscheckparser.update_todos(todos, warnings)
        self.assertEqual(new_todos, expected)

    def test_empty_log(self):
        expected = {}

        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-warnings-good.rst'))
        (compatibles, warnings) = ({}, {})
        new_todos = dtbscheckparser.update_todos(todos, warnings)
        self.assertEqual(new_todos, expected)

    def test_empty_todos_and_log(self):
        expected = {}

        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-warnings-empty.rst'))
        (compatibles, warnings) = ({}, {})
        new_todos = dtbscheckparser.update_todos(todos, warnings)
        self.assertEqual(new_todos, expected)

    def test_good(self):
        expected = {
            "spmi@c440000: #size-cells:0:0: 0 was expected": 'NEW',
            "spmi@c440000: #address-cells:0:0: 2 was expected": 'krzk',
            "pci@1c08000: Unevaluated properties are not allowed ('iommu-map', 'iommus' were unexpected)": 'NEW',
            "phy@1d87000: '#phy-cells' is a required property": 'NEW',
        }

        todos = dtbscheckparser.load_todos(os.path.join(datadir, 'todo-warnings-good.rst'))
        (compatibles, warnings) = dtbscheckparser.parse_platform_warnings(os.path.join(datadir, 'warnings-good.log'),
                                                                          'arch/arm64/boot/dts/', 'qcom')
        new_todos = dtbscheckparser.update_todos(todos, warnings)
        self.assertEqual(new_todos, expected)
