- ['8dev,jalapeno']
  krzk, https://lore.kernel.org/linux-devicetree/20221013155418.47577-1-krzysztof.kozlowski@linaro.org/T/#t
- ['ad,ad7147_captouch']
  ignore
- ['adi,adau7002']
  ignore
- ['alfa-network,ap120c-ac']
  krzk, https://lore.kernel.org/linux-devicetree/20221013155418.47577-1-krzysztof.kozlowski@linaro.org/T/#t
- ['arm,kryo']
  NEW
- ['awinic,aw8695']
  Luca
- ['brcm,bcm43430a0-bt']
  Luca, https://lore.kernel.org/lkml/20220924142154.14217-1-luca@z3ntu.xyz/
- ['brcm,bcm43430a1-bt']
  Luca, https://lore.kernel.org/lkml/20220924142154.14217-1-luca@z3ntu.xyz/
- ['coresight-etb10', 'arm,primecell']
  Luca, https://lore.kernel.org/linux-arm-msm/20221013190657.48499-3-luca@z3ntu.xyz/
- ['dlg,da7280']
  ignore
- ['fcs,fan53526']
  ignore
- ['focaltech,fts8719']
  Caleb, https://lore.kernel.org/all/20220123173650.290349-2-caleb@connolly.tech/
- ['google,cheza', 'qcom,sdm845']
  krzk
- ['google,cheza-rev1', 'qcom,sdm845']
  krzk
- ['google,cheza-rev2', 'qcom,sdm845']
  krzk
- ['google,cr50']
  NEW
- ['jdi,fhd-nt35596s']
  https://lore.kernel.org/all/20220728023555.8952-2-mollysophia379@gmail.com/
- ['lattice,ice40-fpga-mgr']
  ignore
- ['linux,extcon-usb-gpio']
  ignore
- ['maxim,max98360a']
  ignore
- ['mikrotik,rb3011']
  krzk, ipq8064 board compatible
- ['mx25l25635e']
  NEW, spi-nor chip
- ['ovti,ov7251']
  ignore
- ['panasonic,an30259a']
  ignore
- ['pwm-ir-tx']
  Luca
- ['qcom,adc-tm7']
  krzk, https://lore.kernel.org/all/b70df37b-e803-acca-e7e1-5b4526e76776@linaro.org/
- ['qcom,apq8064-ahci', 'generic-ahci']
  NEW
- ['qcom,apq8064-iommu']
  NEW
- ['qcom,apq8064-pinctrl']
  krzk
- ['qcom,apq8064-sata-phy']
  NEW
- ['qcom,apq8084-pinctrl']
  krzk
- ['qcom,apq8096-sndcard']
  ignore
- ['qcom,apss-wdt-msm8994', 'qcom,kpss-wdt']
  NEW
- ['qcom,arch-cache']
  Luca, https://lore.kernel.org/linux-arm-msm/20221013190657.48499-2-luca@z3ntu.xyz/
- ['qcom,ci-hdrc']
  NEW
- ['qcom,crypto-v5.1']
  Bhupesh, https://lore.kernel.org/linux-arm-msm/20220920114051.1116441-2-bhupesh.sharma@linaro.org/
- ['qcom,crypto-v5.4']
  Bhupesh, https://lore.kernel.org/linux-arm-msm/20220920114051.1116441-2-bhupesh.sharma@linaro.org/
- ['qcom,glink-rpm']
  David, seems abandoned, https://lore.kernel.org/all/20220424101637.20721-1-david@ixit.cz/
- ['qcom,hfpll']
  NEW
- ['qcom,idle-state-spc', 'arm,idle-state']
  NEW
- ['qcom,idle-state-spc']
  NEW
- ['qcom,ipq4019-pinctrl']
  krzk
- ['qcom,ipq4019-wifi']
  NEW
- ['qcom,ipq6018-mdio', 'qcom,ipq4019-mdio']
  NEW
- ['qcom,ipq6018-wcss-pil']
  NEW
- ['qcom,ipq8064-pinctrl']
  krzk
- ['qcom,ipq806x-ahci', 'generic-ahci']
  NEW
- ['qcom,ipq806x-gmac', 'snps,dwmac']
  NEW
- ['qcom,ipq806x-sata-phy']
  NEW
- ['qcom,ipq8074-pinctrl']
  krzk
- ['qcom,kpss-acc-v1']
  NEW
- ['qcom,kpss-acc-v2']
  NEW
- ['qcom,kpss-gcc', 'syscon']
  NEW
- ['qcom,kpss-timer', 'qcom,kpss-wdt-apq8064', 'qcom,msm-timer']
  NEW
- ['qcom,kpss-timer', 'qcom,kpss-wdt-ipq8064', 'qcom,msm-timer']
  NEW
- ['qcom,kpss-timer', 'qcom,kpss-wdt-msm8960', 'qcom,msm-timer']
  NEW
- ['qcom,kpss-timer', 'qcom,msm-timer']
  NEW
- ['qcom,lcc-apq8064']
  Luca, https://lore.kernel.org/linux-arm-msm/20221015090946.448820-1-luca@z3ntu.xyz/T/
- ['qcom,lcc-ipq8064']
  Luca, https://lore.kernel.org/linux-arm-msm/20221015090946.448820-1-luca@z3ntu.xyz/T/
- ['qcom,lcc-mdm9615']
  Luca, https://lore.kernel.org/linux-arm-msm/20221015090946.448820-1-luca@z3ntu.xyz/T/
- ['qcom,lcc-msm8960']
  Luca, https://lore.kernel.org/linux-arm-msm/20221015090946.448820-1-luca@z3ntu.xyz/T/
- ['qcom,lpass-cpu-apq8016']
  Bryan?, https://lore.kernel.org/linux-arm-msm/20220908105720.857294-2-bryan.odonoghue@linaro.org/
- ['qcom,mdp5']
  NEW
- ['qcom,mdss']
  NEW
- ['qcom,msm-iommu-v1-ns']
  NEW
- ['qcom,msm-iommu-v1-sec']
  NEW
- ['qcom,msm8660-ebi2']
  NEW
- ['qcom,msm8660-pinctrl']
  krzk
- ['qcom,msm8916-acc']
  NEW
- ['qcom,msm8916-iommu', 'qcom,msm-iommu-v1']
  NEW
- ['qcom,msm8916-mss-pil', 'qcom,q6v5-pil']
  Stephan, https://lore.kernel.org/linux-arm-msm/20220908182433.466908-1-stephan.gerhold@kernkonzept.com/
- ['qcom,msm8916-pinctrl']
  krzk, https://lore.kernel.org/linux-devicetree/20221024002356.28261-1-krzysztof.kozlowski@linaro.org/T/#t
- ['qcom,msm8916-wcd-digital-codec']
  NEW
- ['qcom,msm8953-iommu', 'qcom,msm-iommu-v1']
  NEW
- ['qcom,msm8953-mdp5', 'qcom,mdp5']
  NEW
- ['qcom,msm8960-pinctrl']
  krzk
- ['qcom,msm8974-mss-pil']
  Stephan, https://lore.kernel.org/linux-arm-msm/20220908182433.466908-1-stephan.gerhold@kernkonzept.com/
- ['qcom,msm8974-pinctrl']
  krzk, https://lore.kernel.org/linux-arm-msm/20221017012225.8579-1-krzysztof.kozlowski@linaro.org/T/
- ['qcom,msm8992-pinctrl']
  krzk
- ['qcom,msm8994-pinctrl']
  krzk
- ['qcom,msm8996-mss-pil']
  NEW
- ['qcom,msm8996-pinctrl']
  krzk
- ['qcom,msm8998-mss-pil']
  NEW
- ['qcom,msm8998-pinctrl']
  krzk
- ['qcom,nandcs']
  Luca, https://lore.kernel.org/linux-arm-msm/20221013190657.48499-1-luca@z3ntu.xyz/
- ['qcom,pm8018-pwrkey', 'qcom,pm8921-pwrkey']
  Neil, https://lore.kernel.org/all/20220928-mdm9615-dt-schema-fixes-v3-7-531da552c354@linaro.org/
- ['qcom,pm8058-keypad']
  NEW
- ['qcom,pm8058-keypad-led']
  NEW
- ['qcom,pm8058-led']
  NEW
- ['qcom,pm8058-pwrkey']
  Neil, https://lore.kernel.org/all/20220928-mdm9615-dt-schema-fixes-v3-5-531da552c354@linaro.org/
- ['qcom,pm8916-wcd-analog-codec']
  NEW
- ['qcom,pm8921-keypad']
  NEW
- ['qcom,pm8921-pwrkey']
  Neil, 20220928-mdm9615-dt-schema-fixes-v3-5-531da552c354@linaro.org/
- ['qcom,pm8941-iadc', 'qcom,spmi-iadc']
  Luca, https://lore.kernel.org/linux-arm-msm/20220925161821.78030-1-luca@z3ntu.xyz/ (but approach will probably change)
- ['qcom,pronto-v2-pil', 'qcom,pronto']
  Sireesh, https://lore.kernel.org/linux-arm-msm/20221001031345.31293-3-sireeshkodali1@gmail.com/
- ['qcom,qcs404-ethqos']
  NEW
- ['qcom,qcs404-pcie2-phy', 'qcom,pcie2-phy']
  NEW
- ['qcom,qcs404-pinctrl']
  krzk
- ['qcom,qcs404-turingcc']
  NEW
- ['qcom,riva-pil']
  Sireesh, https://lore.kernel.org/linux-arm-msm/20221001031345.31293-3-sireeshkodali1@gmail.com/
- ['qcom,rpm-apq8064']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-pinctrl-yaml-v2-5-639fe67a04be@linaro.org/
- ['qcom,rpm-ipq8064']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-pinctrl-yaml-v2-5-639fe67a04be@linaro.org/
- ['qcom,rpm-mdm9615']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-pinctrl-yaml-v2-5-639fe67a04be@linaro.org/
- ['qcom,rpm-msm8660']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-pinctrl-yaml-v2-5-639fe67a04be@linaro.org/
- ['qcom,rpm-msm8960']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-pinctrl-yaml-v2-5-639fe67a04be@linaro.org/
- ['qcom,rpm-msm8994']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-pinctrl-yaml-v2-5-639fe67a04be@linaro.org/
- ['qcom,rpm-pm8018-regulators']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-pinctrl-yaml-v2-4-639fe67a04be@linaro.org/
- ['qcom,rpm-pm8058-regulators']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-pinctrl-yaml-v2-4-639fe67a04be@linaro.org/
- ['qcom,rpm-pm8901-regulators']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-pinctrl-yaml-v2-4-639fe67a04be@linaro.org/
- ['qcom,rpm-pm8921-regulators']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-pinctrl-yaml-v2-4-639fe67a04be@linaro.org/
- ['qcom,sc7280-cpu-bwmon', 'qcom,msm8998-bwmon']
  krzk, https://lore.kernel.org/linux-arm-msm/20221011140744.29829-1-krzysztof.kozlowski@linaro.org/T/#u
- ['qcom,sc8280xp-aoss-qmp', 'qcom,aoss-qmp']
  Luca, https://lore.kernel.org/linux-arm-msm/20221016090035.565350-4-luca@z3ntu.xyz/
- ['qcom,sc8280xp-cpufreq-epss', 'qcom,cpufreq-epss']
  Luca, https://lore.kernel.org/linux-arm-msm/20221016090035.565350-1-luca@z3ntu.xyz/T/
- ['qcom,sc8280xp-ipcc', 'qcom,ipcc']
  Luca, https://lore.kernel.org/linux-arm-msm/20221016090035.565350-3-luca@z3ntu.xyz/
- ['qcom,sc8280xp-pdc', 'qcom,pdc']
  Luca, https://lore.kernel.org/lkml/20221013091208.356739-1-luca.weiss@fairphone.com/
- ['qcom,scss-timer', 'qcom,msm-timer']
  NEW
- ['qcom,sdm630-smmu-v2', 'qcom,adreno-smmu', 'qcom,smmu-v2']
  NEW
- ['qcom,sdm630-smmu-v2', 'qcom,smmu-v2']
  NEW
- ['qcom,sdm845-lpasscc']
  krzk
- ['qcom,sdm845-mss-pil']
  NEW
- ['qcom,sdx55-pdc', 'qcom,pdc']
  Luca, https://lore.kernel.org/lkml/20221013091208.356739-1-luca.weiss@fairphone.com/
- ['qcom,sdx65-pdc', 'qcom,pdc']
  Luca, https://lore.kernel.org/lkml/20221013091208.356739-1-luca.weiss@fairphone.com/
- ['qcom,slim-ngd-v1.5.0']
  NEW
- ['qcom,slim-ngd-v2.1.0']
  NEW
- ['qcom,sm8150-camnoc-virt']
  NEW
- ['qcom,sm8150-ethqos']
  NEW
- ['qcom,sm8150-qmp-gen3x1-pcie-phy']
  Bhupesh (from 2022-03), https://lore.kernel.org/linux-arm-msm/20220325222130.1783242-2-bhupesh.sharma@linaro.org/
- ['qcom,sm8150-qmp-gen3x2-pcie-phy']
  Bhupesh (from 2022-03), https://lore.kernel.org/linux-arm-msm/20220325222130.1783242-2-bhupesh.sharma@linaro.org/
- ['qcom,sm8250-dpu']
  NEW
- ['qcom,sm8250-lpass-aoncc']
  krzk
- ['qcom,sm8250-mdss']
  NEW
- ['qcom,sm8350-cpufreq-epss', 'qcom,cpufreq-epss']
  Luca, https://lore.kernel.org/linux-arm-msm/20221016090035.565350-1-luca@z3ntu.xyz/T/
- ['qcom,sm8450-cpufreq-epss', 'qcom,cpufreq-epss']
  Luca, https://lore.kernel.org/linux-arm-msm/20221016090035.565350-1-luca@z3ntu.xyz/T/
- ['qcom,sm8450-pdc', 'qcom,pdc']
  Luca, https://lore.kernel.org/lkml/20221013091208.356739-1-luca.weiss@fairphone.com/
- ['qcom,sm8450-tsens', 'qcom,tsens-v2']
  Luca, https://lore.kernel.org/linux-arm-msm/20221016090035.565350-5-luca@z3ntu.xyz/
- ['qcom,soundwire-v1.3.0']
  Srinivasa Rao Mandadapu, https://lore.kernel.org/all/1666271160-22424-1-git-send-email-quic_srivasam@quicinc.com/
- ['qcom,soundwire-v1.5.1']
  Srinivasa Rao Mandadapu, https://lore.kernel.org/all/1666271160-22424-1-git-send-email-quic_srivasam@quicinc.com/
- ['qcom,soundwire-v1.6.0']
  Srinivasa Rao Mandadapu, https://lore.kernel.org/all/1666271160-22424-1-git-send-email-quic_srivasam@quicinc.com/
- ['qcom,ssbi']
  Dmitry, https://lore.kernel.org/all/20220930212052.894834-1-dmitry.baryshkov@linaro.org/
- ['qcom,ufs_variant']
  Luca, https://lore.kernel.org/lkml/20221012215613.32054-1-luca@z3ntu.xyz/T/
- ['qcom,wcn3620']
  NEW
- ['qcom,wcn3660']
  NEW
- ['qcom,wcn3660b']
  NEW
- ['qcom,wcn3680']
  NEW
- ['qcom,wcn3990-wifi']
  NEW
- ['realtek,rt5682i']
  ignore
- ['removed-dma-pool', 'ramoops']
  Luca
- ['samsung,s6e3fa2']
  https://lore.kernel.org/all/20210725140339.2465677-1-alexeymin@postmarketos.org/
- ['samsung,s6sy761']
  ignore
- ['semtech,sx1509q']
  Neil, https://lore.kernel.org/all/20221005-mdm9615-sx1509q-yaml-v2-0-a4a5b8eecc7b@linaro.org/
- ['silabs,si470x']
  ignore
- ['slim217,1a0']
  ignore
- ['swir,mangoh-green-wp8548', 'swir,wp8548', 'qcom,mdm9615']
  Neil, https://lore.kernel.org/all/20220928-mdm9615-dt-schema-fixes-v3-1-531da552c354@linaro.org/
- ['swir,mangoh-iotport-spi']
  NEW
- ['syna,rmi4-i2c']
  ignore
- ['ti,drv2604']
  Luca, https://lore.kernel.org/lkml/20221012214219.28976-1-luca@z3ntu.xyz/T/
- ['ti,lp8556']
  ignore
- ['ti,tas2552']
  ignore
- ['ti,tps65132']
  ignore
- ['truly,nt35597-2K-display']
  ignore
- ['visionox,rm69299-shift']
  Caleb, https://lore.kernel.org/all/20220123173650.290349-4-caleb@connolly.tech/
