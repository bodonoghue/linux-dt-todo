- /: cpus:cpu@0: 'power-domain-names' is a required property
  NEW
- /: cpus:cpu@0: 'power-domains' is a required property
  NEW
- /: cpus:cpu@1: 'power-domain-names' is a required property
  NEW
- /: cpus:cpu@1: 'power-domains' is a required property
  NEW
- /: cpus:cpu@2: 'power-domain-names' is a required property
  NEW
- /: cpus:cpu@2: 'power-domains' is a required property
  NEW
- /: cpus:cpu@3: 'power-domain-names' is a required property
  NEW
- /: cpus:cpu@3: 'power-domains' is a required property
  NEW
- /: memory: False schema does not allow {'device_type': ['memory'], 'reg': [[0, 0]]}
  NEW
- amba: $nodename:0: 'amba' does not match '^([a-z][a-z0-9\\-]+-bus|bus|localbus|soc|axi|ahb|apb)(@.+)?$'
  NEW
- clock-controller@100000: Unevaluated properties are not allowed ('power-domains' was unexpected)
  NEW
- domain-idle-states: cluster-sleep-0: 'idle-state-name', 'local-timer-stop' do not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- dsi@ae94000: 'opp-table', 'vdda-supply' do not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- dsi@ae94000: 'phy-names' is a required property
  NEW
- idle-states: 'spc' does not match any of the regexes: '^(cpu|cluster)-', 'pinctrl-[0-9]+'
  NEW
- leds@d800: interrupt-names: ['ovp'] is too short
  NEW
- leds@d800: interrupts: [[5, 216, 1, 1]] is too short
  NEW
- lpass@62d87000: 'hdmi@5', 'mi2s@0', 'mi2s@1' do not match any of the regexes: '^dai-link@[0-9a-f]$', 'pinctrl-[0-9]+'
  NEW
- mdss@ae00000: 'displayport-controller@ae90000', 'dsi@ae94000', 'phy@ae94400' do not match any of the regexes: '^display-controller@[0-9a-f]+$', 'pinctrl-[0-9]+'
  NEW
- memory@fc428000: $nodename:0: 'memory@fc428000' does not match '^sram(@.*)?'
  NEW
- mmc@12180000: Unevaluated properties are not allowed ('interrupt-names' was unexpected)
  NEW
- mmc@12400000: Unevaluated properties are not allowed ('interrupt-names' was unexpected)
  NEW
- pci@1c08000: Unevaluated properties are not allowed ('iommu-map', 'iommus' were unexpected)
  NEW
- phy@1d87000: '#phy-cells' is a required property
  NEW
- phy@88e3000: 'qcom,bias-ctrl-value', 'qcom,charge-ctrl-value', 'qcom,hsdisc-trim-value', 'qcom,imp-res-offset-value', 'qcom,preemphasis-level', 'qcom,preemphasis-width' do not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- pinctrl@3500000: qup-spi0-cs-gpio: {'phandle': [[52]], 'pinmux': {'pins': ['gpio34', 'gpio35', 'gpio36'], 'function': ['qup00']}, 'pinmux-cs': {'pins': ['gpio37'], 'function': ['gpio']}, 'pinconf': {'pins': ['gpio34', 'gpio35', 'gpio36', 'gpio37'], 'drive-strength': [[2]], 'bias-disable': True}} is not of type 'array'
  NEW
- pinctrl@3500000: qup-spi1-cs-gpio: {'pinmux': {'pins': ['gpio0', 'gpio1', 'gpio2'], 'function': ['qup01']}, 'pinmux-cs': {'pins': ['gpio3'], 'function': ['gpio']}} is not of type 'array'
  NEW
- pinctrl@3500000: qup-spi11-cs-gpio: {'pinmux': {'pins': ['gpio53', 'gpio54', 'gpio55'], 'function': ['qup15']}, 'pinmux-cs': {'pins': ['gpio56'], 'function': ['gpio']}} is not of type 'array'
  NEW
- pinctrl@3500000: qup-spi3-cs-gpio: {'pinmux': {'pins': ['gpio38', 'gpio39', 'gpio40'], 'function': ['qup03']}, 'pinmux-cs': {'pins': ['gpio41'], 'function': ['gpio']}} is not of type 'array'
  NEW
- pinctrl@3500000: qup-spi5-cs-gpio: {'pinmux': {'pins': ['gpio25', 'gpio26', 'gpio27'], 'function': ['qup05']}, 'pinmux-cs': {'pins': ['gpio28'], 'function': ['gpio']}} is not of type 'array'
  NEW
- pinctrl@3500000: qup-spi8-cs-gpio: {'pinmux': {'pins': ['gpio42', 'gpio43', 'gpio44'], 'function': ['qup12']}, 'pinmux-cs': {'pins': ['gpio45'], 'function': ['gpio']}} is not of type 'array'
  NEW
- pmic@1: 'pwm' does not match any of the regexes: '(.*)?(wled|leds)@[0-9a-f]+$', '^adc-tm@[0-9a-f]+$', '^adc@[0-9a-f]+$', '^audio-codec@[0-9a-f]+$', '^mpps@[0-9a-f]+$', '^rtc@[0-9a-f]+$', '^temp-alarm@[0-9a-f]+$', '^vibrator@[0-9a-f]+$', 'extcon@[0-9a-f]+$', 'gpio(s)?@[0-9a-f]+$', 'pinctrl-[0-9]+', 'pon@[0-9a-f]+$', 'pwm@[0-9a-f]+$'
  NEW
- pmic@3: 'pwm' does not match any of the regexes: '(.*)?(wled|leds)@[0-9a-f]+$', '^adc-tm@[0-9a-f]+$', '^adc@[0-9a-f]+$', '^audio-codec@[0-9a-f]+$', '^mpps@[0-9a-f]+$', '^rtc@[0-9a-f]+$', '^temp-alarm@[0-9a-f]+$', '^vibrator@[0-9a-f]+$', 'extcon@[0-9a-f]+$', 'gpio(s)?@[0-9a-f]+$', 'pinctrl-[0-9]+', 'pon@[0-9a-f]+$', 'pwm@[0-9a-f]+$'
  NEW
- pmic@5: leds@d800:interrupt-names: ['ovp'] is too short
  NEW
- pmic@5: leds@d800:interrupts: [[5, 216, 1, 1]] is too short
  NEW
- power-controller@c300000: '#power-domain-cells' is a required property
  NEW
- power-controller@f9012000: '#power-domain-cells' is a required property
  NEW
- power-controller@f9012000: 'regulator' does not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- power-controller@f9012000: compatible: ['qcom,saw2'] is too short
  NEW
- power-controller@f9012000: compatible:0: 'qcom,saw2' is not one of ['qcom,sdm660-gold-saw2-v4.1-l2', 'qcom,sdm660-silver-saw2-v4.1-l2', 'qcom,msm8998-gold-saw2-v4.1-l2', 'qcom,msm8998-silver-saw2-v4.1-l2', 'qcom,msm8909-saw2-v3.0-cpu', 'qcom,msm8916-saw2-v3.0-cpu', 'qcom,msm8226-saw2-v2.1-cpu', 'qcom,msm8974-saw2-v2.1-cpu', 'qcom,apq8084-saw2-v2.1-cpu', 'qcom,apq8064-saw2-v1.1-cpu']
  NEW
- power-controller@f9089000: '#power-domain-cells' is a required property
  NEW
- power-controller@f9089000: reg: [[4178087936, 4096], [4177563648, 4096]] is too long
  NEW
- power-controller@f9099000: '#power-domain-cells' is a required property
  NEW
- power-controller@f9099000: reg: [[4178153472, 4096], [4177563648, 4096]] is too long
  NEW
- power-controller@f90a9000: '#power-domain-cells' is a required property
  NEW
- power-controller@f90a9000: reg: [[4178219008, 4096], [4177563648, 4096]] is too long
  NEW
- power-controller@f90b9000: '#power-domain-cells' is a required property
  NEW
- power-controller@f90b9000: reg: [[4178284544, 4096], [4177563648, 4096]] is too long
  NEW
- proximity@28: 'label' does not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- pwmleds: 'keyboard-backlight' does not match any of the regexes: '^led(-[0-9a-f]+)?$', 'pinctrl-[0-9]+'
  NEW
- pwrkey@800: Unevaluated properties are not allowed ('reg' was unexpected)
  NEW
- smd: rpm: Unevaluated properties are not allowed ('rpm-requests' was unexpected)
  NEW
- soc@0: opp-table-qup: {'compatible': ['operating-points-v2'], 'phandle': [[53]], 'opp-75000000': {'opp-hz': [[0], [75000000]], 'required-opps': [[46]]}, 'opp-100000000': {'opp-hz': [[0], [100000000]], 'required-opps': [[48]]}, 'opp-128000000': {'opp-hz': [[0], [128000000]], 'required-opps': [[47]]}} should not be valid under {'type': 'object'}
  NEW
- spi@88dc000: Unevaluated properties are not allowed ('operating-points-v2', 'power-domains' were unexpected)
  NEW
- spmi@c440000: #address-cells:0:0: 2 was expected
  NEW
- spmi@c440000: #size-cells:0:0: 0 was expected
  NEW
- spmi@c440000: Unevaluated properties are not allowed ('cell-index', 'pmic@0', 'pmic@1', 'pmic@4', 'pmic@5' were unexpected)
  NEW
- syscon@f9011000: compatible: 'anyOf' conditional failed, one must be fixed:
  NEW
- usb2@60f8800: clock-names:0: 'core' was expected
  NEW
- usb3@8af8800: 'dwc3@8a00000' does not match any of the regexes: '^usb@[0-9a-f]+$', 'pinctrl-[0-9]+'
  NEW
- usb@a6f8800: 'dma-ranges', 'required-opps' do not match any of the regexes: '^usb@[0-9a-f]+$', 'pinctrl-[0-9]+'
  NEW
- video-codec@aa00000: 'operating-points-v2', 'opp-table' do not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- watchdog@17c10000: Unevaluated properties are not allowed ('interrupts' was unexpected)
  NEW
- watchdog@b017000: Unevaluated properties are not allowed ('compatible' was unexpected)
  NEW
- watchdog@b017000: compatible: 'oneOf' conditional failed, one must be fixed:
  NEW
