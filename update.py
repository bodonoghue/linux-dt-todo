#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-only
# Copyright 2022 Linaro Ltd

import dtbscheckparser

OUTPUT_TODO_COMPATIBLES = 'todo-compatibles.rst'
OUTPUT_TODO_WARNINGS = 'todo-warnings.rst'

if __name__ == '__main__':
    todo_compatibles = dtbscheckparser.load_todos(OUTPUT_TODO_COMPATIBLES)
    todo_warnings = dtbscheckparser.load_todos(OUTPUT_TODO_WARNINGS)

    (compatibles, warnings) = dtbscheckparser.parse_platform_warnings('data/arm64/platform-warnings.log',
                                                                      'arch/arm64/boot/dts/', 'qcom')

    (compatibles2, warnings2) = dtbscheckparser.parse_platform_warnings('data/arm/platform-warnings.log',
                                                                        'arch/arm/boot/dts/', 'qcom')

    compatibles.extend(compatibles2)
    compatibles.sort()
    warnings.extend(warnings2)
    warnings.sort()

    new_todos = dtbscheckparser.update_todos(todo_compatibles, compatibles)
    with open(OUTPUT_TODO_COMPATIBLES, 'w') as fp:
        for key, value in new_todos.items():
            fp.write('- {}\n'.format(key))
            fp.write('  {}\n'.format(value))

    new_todos = dtbscheckparser.update_todos(todo_warnings, warnings)
    with open(OUTPUT_TODO_WARNINGS, 'w') as fp:
        for key, value in new_todos.items():
            fp.write('- {}\n'.format(key))
            fp.write('  {}\n'.format(value))
