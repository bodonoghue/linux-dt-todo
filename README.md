# Qualcomm bindings: TODO list and tasks

1. See files: todo-compatibles.rst and todo-warnings.rst

2. Feel free to update them with current status or assign yourself (to avoid duplicating effort).

3. Test parsing of both TODO files:

       ./test.sh


# Updating the list

1. Drop the artifacts from Rob's dtbs_check jobs into data/

   https://gitlab.com/robherring/linux-dt/-/jobs

2. Update the TODO files:

       ./update.py

3. Commit
